// Try paiza Phantom Thief
// author: Leonardone @ NEETSDKASU
package main

import (
	"fmt"
	"math"
	"math/rand"
	"sort"
	"time"
)

type Pos struct{ x, y int }

func (a *Pos) Distance(b Pos) float64 {
	dx := float64(a.x - b.x)
	dy := float64(a.y - b.y)
	return math.Sqrt(dx*dx + dy*dy)
}

func DistanceNeighbours(poss []Pos, i int) float64 {
	return poss[i].Distance(poss[i-1]) + poss[i].Distance(poss[i+1])
}

func Swap(poss []Pos, i, j int) {
	poss[i], poss[j] = poss[j], poss[i]
	if n := len(poss); j == n-2 {
		poss[n-1] = poss[j]
	}
}

func OrderToNearestCurrent(poss []Pos) (res []Pos) {
	current := Pos{0, 0}
	for len(poss) > 0 {
		sort.Slice(poss, func(i, j int) bool {
			di := current.Distance(poss[i])
			dj := current.Distance(poss[j])
			return di < dj
		})
		current = poss[0]
		res = append(res, current)
		poss = poss[1:]
	}
	return
}

func calcScore(poss []Pos) float64 {
	score := float64(0)
	for i, p := range poss[1:] {
		score += p.Distance(poss[i])
	}
	return score
}

func Swapping2(n int, temp []Pos) {
	for t := 0; t < 5000; t++ {
		maximum := 0.0
		swapPos := Pos{}
		for i := 1; i < n; i++ {
			di := DistanceNeighbours(temp, i)
			for j := i + 1; j <= n; j++ {
				dist := di + DistanceNeighbours(temp, j)
				Swap(temp, i, j)
				dist -= DistanceNeighbours(temp, i) + DistanceNeighbours(temp, j)
				Swap(temp, i, j)
				if math.Signbit(dist) {
					continue
				}
				if dist == math.Max(dist, maximum) {
					maximum = dist
					swapPos = Pos{i, j}
				}
			}
		}
		if swapPos.x == 0 {
			break
		}
		Swap(temp, swapPos.x, swapPos.y)
	}
}

func Swapping3A(n int, temp []Pos) {
	for t := 0; t < 5000; t++ {
		maximum := 0.0
		swapPos := make([]Pos, 2)
		for i := 1; i < n-1; i++ {
			di := DistanceNeighbours(temp, i)
			for j := i + 1; j < n; j++ {
				dij := di + DistanceNeighbours(temp, j)
				for k := j + 1; k <= n; k++ {
					dist := dij + DistanceNeighbours(temp, k)
					Swap(temp, i, j)
					Swap(temp, i, k)
					dist -= DistanceNeighbours(temp, i) + DistanceNeighbours(temp, j) + DistanceNeighbours(temp, k)
					Swap(temp, i, k)
					Swap(temp, i, j)
					if math.Signbit(dist) {
						continue
					}
					if dist == math.Max(dist, maximum) {
						maximum = dist
						swapPos[0] = Pos{i, j}
						swapPos[1] = Pos{i, k}
					}
				}
			}
		}
		if swapPos[0].x == 0 {
			break
		}
		for _, sp := range swapPos {
			Swap(temp, sp.x, sp.y)
		}
	}
}
func Swapping3B(n int, temp []Pos) {
	for t := 0; t < 5000; t++ {
		maximum := 0.0
		swapPos := make([]Pos, 2)
		for i := 1; i < n-1; i++ {
			di := DistanceNeighbours(temp, i)
			for j := i + 1; j < n; j++ {
				dij := di + DistanceNeighbours(temp, j)
				for k := j + 1; k <= n; k++ {
					dist := dij + DistanceNeighbours(temp, k)
					Swap(temp, i, k)
					Swap(temp, i, j)
					dist -= DistanceNeighbours(temp, i) + DistanceNeighbours(temp, j) + DistanceNeighbours(temp, k)
					Swap(temp, i, j)
					Swap(temp, i, k)
					if math.Signbit(dist) {
						continue
					}
					if dist == math.Max(dist, maximum) {
						maximum = dist
						swapPos[0] = Pos{i, k}
						swapPos[1] = Pos{i, j}
					}
				}
			}
		}
		if swapPos[0].x == 0 {
			break
		}
		for _, sp := range swapPos {
			Swap(temp, sp.x, sp.y)
		}
	}
}

func Solve(poss []Pos) (ans []Pos) {
	limit := time.After(2950 * time.Millisecond)
	poss = OrderToNearestCurrent(poss)
	n := len(poss)
	if n == 2 {
		ans = poss
		return
	}
	ans = make([]Pos, n)
	copy(ans, poss)
	temp := []Pos{{0, 0}}
	temp = append(temp, poss...)
	temp = append(temp, poss[n-1])
	bestScore := calcScore(temp)
	startScore := bestScore
	score := bestScore
	rand.Seed(19831983)
	const maxloop = 5000000
	const Alpha = 0.0008
	firstlimit := time.After(1000 * time.Millisecond)
mainloop:
	for t := 0; t < maxloop; t++ {
		select {
		case <-firstlimit:
			break mainloop
		default:
		}

		i := rand.Intn(n) + 1
		j := rand.Intn(n) + 1
		if i == j {
			j = i%n + 1
		}
		k := rand.Intn(n) + 1
		if i == k {
			k = i%n + 1
		}
		if j == k {
			k = j%n + 1
		}
		Swap(temp, i, j)
		Swap(temp, j, k)
		tmpScore := calcScore(temp)
		if tmpScore < bestScore {
			score = tmpScore
			startScore = tmpScore
			bestScore = tmpScore
			copy(ans, temp[1:n+1])
			continue
		}
		if tmpScore < startScore {
			score = tmpScore
			startScore = tmpScore
			continue
		}
		te := math.Pow(Alpha, float64(t)/maxloop)
		ee := math.Exp((startScore - tmpScore) / te)
		if rand.Float64() < ee {
			score = tmpScore
			startScore = tmpScore
			continue
		}
		Swap(temp, j, k)
		Swap(temp, i, j)
	}

	Swapping2(n, temp)
	Swapping3A(n, temp)
	Swapping3B(n, temp)

	score = calcScore(temp)
	startScore = score
	if score < bestScore {
		bestScore = score
		copy(ans, temp[1:n+1])
	}

	secondlimit := time.After(500 * time.Millisecond)
secondloop:
	for t := 0; t < maxloop; t++ {
		select {
		case <-secondlimit:
			break secondloop
		default:
		}

		i := rand.Intn(n) + 1
		j := rand.Intn(n) + 1
		k := rand.Intn(n) + 1
		if i == k {
			k = i%n + 1
		}
		Swap(temp, i, j)
		Swap(temp, j, k)
		tmpScore := calcScore(temp)
		if tmpScore < bestScore {
			score = tmpScore
			startScore = tmpScore
			bestScore = tmpScore
			copy(ans, temp[1:n+1])
			continue
		}
		if tmpScore < startScore {
			score = tmpScore
			startScore = tmpScore
			continue
		}
		te := math.Pow(Alpha, float64(t)/maxloop)
		ee := math.Exp((startScore - tmpScore) / te)
		if rand.Float64() < ee {
			score = tmpScore
			startScore = tmpScore
			continue
		}
		Swap(temp, j, k)
		Swap(temp, i, j)
	}

	Swapping2(n, temp)
	Swapping3A(n, temp)
	Swapping3B(n, temp)

	score = calcScore(temp)
	startScore = score
	if score < bestScore {
		bestScore = score
		copy(ans, temp[1:n+1])
	}
thirdloop:
	for t := 0; t < maxloop; t++ {
		select {
		case <-limit:
			break thirdloop
		default:
		}

		i := rand.Intn(n) + 1
		j := rand.Intn(n) + 1
		k := rand.Intn(n) + 1
		if i == k {
			k = i%n + 1
		}
		Swap(temp, i, j)
		Swap(temp, j, k)
		tmpScore := calcScore(temp)
		if tmpScore < bestScore {
			score = tmpScore
			startScore = tmpScore
			bestScore = tmpScore
			copy(ans, temp[1:n+1])
			continue
		}
		if tmpScore < startScore {
			score = tmpScore
			startScore = tmpScore
			continue
		}
		te := math.Pow(Alpha, float64(t)/maxloop)
		ee := math.Exp((startScore - tmpScore) / te)
		if rand.Float64() < ee {
			score = tmpScore
			startScore = tmpScore
			continue
		}
		Swap(temp, j, k)
		Swap(temp, i, j)
	}

	score = calcScore(temp)
	if score < bestScore {
		copy(ans, temp[1:n+1])
	}
	return
}

func main() {
	var n int
	fmt.Scan(&n)
	poss := make([]Pos, n)
	for i := range poss {
		fmt.Scan(&poss[i].x, &poss[i].y)
	}
	ans := Solve(poss)
	for _, p := range ans {
		fmt.Println(p.x, p.y)
	}
}
