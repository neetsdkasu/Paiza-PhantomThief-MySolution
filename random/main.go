// Try paiza Phantom Thief
// author: Leonardone @ NEETSDKASU
package main

import (
	"fmt"
	"math/rand"
)

type Pos struct{ x, y int }

func Solve(poss []Pos) (ans []Pos) {
	ans = poss
	for i := range ans[:len(ans)-1] {
		j := rand.Intn((len(ans)-1)-i) + i
		ans[i], ans[j] = ans[j], ans[i]
	}
	return
}

func main() {
	var n int
	fmt.Scan(&n)
	poss := make([]Pos, n)
	for i := range poss {
		fmt.Scan(&poss[i].x, &poss[i].y)
	}
	ans := Solve(poss)
	for _, p := range ans {
		fmt.Println(p.x, p.y)
	}
}
