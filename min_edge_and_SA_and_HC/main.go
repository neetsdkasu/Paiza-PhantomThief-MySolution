// Try paiza Phantom Thief
// author: Leonardone @ NEETSDKASU
package main

import (
	"fmt"
	"math"
	"math/rand"
	"time"
)

type Pos struct{ x, y int }

func (a *Pos) Distance(b Pos) float64 {
	dx := float64(a.x - b.x)
	dy := float64(a.y - b.y)
	return math.Sqrt(dx*dx + dy*dy)
}

func DistanceNeighbours(poss []Pos, i int) float64 {
	return poss[i].Distance(poss[i-1]) + poss[i].Distance(poss[i+1])
}

func Swap(poss []Pos, i, j int) {
	poss[i], poss[j] = poss[j], poss[i]
	if n := len(poss); j == n-2 {
		poss[n-1] = poss[j]
	}
}

type Path []int

type Edge struct {
	f, t       int
	flas, tlas bool
	dis        float64
}

func OrderToMinEdge(poss []Pos) (res []Pos) {
	n := len(poss)
	poss = append(poss, Pos{0, 0})

	tmp := []Path{}
	for i := range poss {
		tmp = append(tmp, Path{i})
	}

	for len(tmp) > 1 {
		var e Edge
		e.dis = math.MaxFloat64
		for i, t1 := range tmp {
			for j, t2 := range tmp[i+1:] {
				d1 := poss[t1[0]].Distance(poss[t2[0]])
				if d1 < e.dis {
					e = Edge{f: i, t: j + i + 1, dis: d1}
				}
				if t1[len(t1)-1] != n {
					d2 := poss[t1[len(t1)-1]].Distance(poss[t2[0]])
					if d2 < e.dis {
						e = Edge{f: i, t: j + i + 1, flas: true, dis: d2}
					}
				}
				if t2[len(t2)-1] != n {
					d3 := poss[t1[0]].Distance(poss[t2[len(t2)-1]])
					if d3 < e.dis {
						e = Edge{f: i, t: j + i + 1, tlas: true, dis: d3}
					}
				}
				if t1[len(t1)-1] != n && t2[len(t2)-1] != n {
					d4 := poss[t1[len(t1)-1]].Distance(poss[t2[len(t2)-1]])
					if d4 < e.dis {
						e = Edge{f: i, t: j + i + 1, flas: true, tlas: true, dis: d4}
					}
				}
			}
		}
		tmp2 := []Path{}
		for i, t := range tmp {
			if e.f != i && e.t != i {
				tmp2 = append(tmp2, t)
			}
		}
		switch {
		case !e.flas && !e.tlas:
			if tmp[e.f][len(tmp[e.f])-1] == n {
				e.f, e.t = e.t, e.f
			}
			xs := tmp[e.f]
			for i, j := 0, len(xs)-1; i < j; i, j = i+1, j-1 {
				xs[i], xs[j] = xs[j], xs[i]
			}
			xs = append(xs, tmp[e.t]...)
			tmp2 = append(tmp2, xs)
		case e.flas && !e.tlas:
			xs := append(tmp[e.f], tmp[e.t]...)
			tmp2 = append(tmp2, xs)
		case !e.flas && e.tlas:
			xs := append(tmp[e.t], tmp[e.f]...)
			tmp2 = append(tmp2, xs)
		case e.flas && e.tlas:
			xs := tmp[e.f]
			for i := range tmp[e.t] {
				xs = append(xs, tmp[e.t][len(tmp[e.t])-1-i])
			}
			tmp2 = append(tmp2, xs)
		}
		tmp = tmp2
	}

	for i := n - 1; i >= 0; i-- {
		res = append(res, poss[tmp[0][i]])
	}
	return
}

func calcScore(poss []Pos) float64 {
	score := float64(0)
	for i, p := range poss[1:] {
		score += p.Distance(poss[i])
	}
	return score
}

func Swapping2(n int, temp []Pos) {
	for t := 0; t < 5000; t++ {
		maximum := 0.0
		swapPos := Pos{}
		for i := 1; i < n; i++ {
			di := DistanceNeighbours(temp, i)
			for j := i + 1; j <= n; j++ {
				dist := di + DistanceNeighbours(temp, j)
				Swap(temp, i, j)
				dist -= DistanceNeighbours(temp, i) + DistanceNeighbours(temp, j)
				Swap(temp, i, j)
				if math.Signbit(dist) {
					continue
				}
				if dist == math.Max(dist, maximum) {
					maximum = dist
					swapPos = Pos{i, j}
				}
			}
		}
		if swapPos.x == 0 {
			break
		}
		Swap(temp, swapPos.x, swapPos.y)
	}
}

func Swapping3A(n int, temp []Pos) {
	for t := 0; t < 5000; t++ {
		maximum := 0.0
		swapPos := make([]Pos, 2)
		for i := 1; i < n-1; i++ {
			di := DistanceNeighbours(temp, i)
			for j := i + 1; j < n; j++ {
				dij := di + DistanceNeighbours(temp, j)
				for k := j + 1; k <= n; k++ {
					dist := dij + DistanceNeighbours(temp, k)
					Swap(temp, i, j)
					Swap(temp, i, k)
					dist -= DistanceNeighbours(temp, i) + DistanceNeighbours(temp, j) + DistanceNeighbours(temp, k)
					Swap(temp, i, k)
					Swap(temp, i, j)
					if math.Signbit(dist) {
						continue
					}
					if dist == math.Max(dist, maximum) {
						maximum = dist
						swapPos[0] = Pos{i, j}
						swapPos[1] = Pos{i, k}
					}
				}
			}
		}
		if swapPos[0].x == 0 {
			break
		}
		for _, sp := range swapPos {
			Swap(temp, sp.x, sp.y)
		}
	}
}
func Swapping3B(n int, temp []Pos) {
	for t := 0; t < 5000; t++ {
		maximum := 0.0
		swapPos := make([]Pos, 2)
		for i := 1; i < n-1; i++ {
			di := DistanceNeighbours(temp, i)
			for j := i + 1; j < n; j++ {
				dij := di + DistanceNeighbours(temp, j)
				for k := j + 1; k <= n; k++ {
					dist := dij + DistanceNeighbours(temp, k)
					Swap(temp, i, k)
					Swap(temp, i, j)
					dist -= DistanceNeighbours(temp, i) + DistanceNeighbours(temp, j) + DistanceNeighbours(temp, k)
					Swap(temp, i, j)
					Swap(temp, i, k)
					if math.Signbit(dist) {
						continue
					}
					if dist == math.Max(dist, maximum) {
						maximum = dist
						swapPos[0] = Pos{i, k}
						swapPos[1] = Pos{i, j}
					}
				}
			}
		}
		if swapPos[0].x == 0 {
			break
		}
		for _, sp := range swapPos {
			Swap(temp, sp.x, sp.y)
		}
	}
}

func Solve(poss []Pos) (ans []Pos) {
	limit := time.After(2950 * time.Millisecond)
	poss = OrderToMinEdge(poss)
	n := len(poss)
	if n == 2 {
		ans = poss
		return
	}
	ans = make([]Pos, n)
	copy(ans, poss)
	temp := []Pos{{0, 0}}
	temp = append(temp, poss...)
	temp = append(temp, poss[n-1])
	bestScore := calcScore(temp)
	startScore := bestScore
	score := bestScore
	rand.Seed(19831983)
	const maxloop = 500000
	const maxloopX = 5000000
	const Alpha = 0.0008
	const AlphaX = 0.08
	firstlimit := time.After(1000 * time.Millisecond)
mainloop:
	for t := 0; t < maxloop; t++ {
		select {
		case <-firstlimit:
			break mainloop
		default:
		}

		i := rand.Intn(n) + 1
		j := rand.Intn(n) + 1
		if i == j {
			j = i%n + 1
		}
		k := rand.Intn(n) + 1
		if i == k {
			k = i%n + 1
		}
		if j == k {
			k = j%n + 1
		}
		Swap(temp, i, j)
		Swap(temp, j, k)
		tmpScore := calcScore(temp)
		if tmpScore < bestScore {
			score = tmpScore
			startScore = tmpScore
			bestScore = tmpScore
			copy(ans, temp[1:n+1])
			continue
		}
		if tmpScore < startScore {
			score = tmpScore
			startScore = tmpScore
			continue
		}
		te := math.Pow(Alpha, float64(t)/maxloop)
		ee := math.Exp((startScore - tmpScore) / te)
		if rand.Float64() < ee {
			score = tmpScore
			startScore = tmpScore
			continue
		}
		Swap(temp, j, k)
		Swap(temp, i, j)
	}

	Swapping2(n, temp)
	Swapping3A(n, temp)
	Swapping3B(n, temp)

	score = calcScore(temp)
	startScore = score
	if score < bestScore {
		bestScore = score
		copy(ans, temp[1:n+1])
	}

	secondlimit := time.After(500 * time.Millisecond)
secondloop:
	for t := 0; t < maxloop; t++ {
		select {
		case <-secondlimit:
			break secondloop
		default:
		}

		i := rand.Intn(n) + 1
		j := rand.Intn(n) + 1
		k := rand.Intn(n) + 1
		if i == k {
			k = i%n + 1
		}
		Swap(temp, i, j)
		Swap(temp, j, k)
		tmpScore := calcScore(temp)
		if tmpScore < bestScore {
			score = tmpScore
			startScore = tmpScore
			bestScore = tmpScore
			copy(ans, temp[1:n+1])
			continue
		}
		if tmpScore < startScore {
			score = tmpScore
			startScore = tmpScore
			continue
		}
		te := math.Pow(Alpha, float64(t)/maxloop)
		ee := math.Exp((startScore - tmpScore) / te)
		if rand.Float64() < ee {
			score = tmpScore
			startScore = tmpScore
			continue
		}
		Swap(temp, j, k)
		Swap(temp, i, j)
	}

	Swapping2(n, temp)
	Swapping3A(n, temp)
	Swapping3B(n, temp)

	score = calcScore(temp)
	startScore = score
	if score < bestScore {
		bestScore = score
		copy(ans, temp[1:n+1])
	}
thirdloop:
	for t := 0; t < maxloopX; t++ {
		select {
		case <-limit:
			break thirdloop
		default:
		}

		i := rand.Intn(n) + 1
		j := rand.Intn(n) + 1
		k := rand.Intn(n) + 1
		if i == k {
			k = i%n + 1
		}
		Swap(temp, i, j)
		Swap(temp, j, k)
		tmpScore := calcScore(temp)
		if tmpScore < bestScore {
			score = tmpScore
			startScore = tmpScore
			bestScore = tmpScore
			copy(ans, temp[1:n+1])
			continue
		}
		if tmpScore < startScore {
			score = tmpScore
			startScore = tmpScore
			continue
		}
		te := math.Pow(AlphaX, float64(t)/maxloopX)
		ee := math.Exp((startScore - tmpScore) / te / startScore)
		if rand.Float64() < ee {
			score = tmpScore
			startScore = tmpScore
			continue
		}
		Swap(temp, j, k)
		Swap(temp, i, j)
	}

	score = calcScore(temp)
	if score < bestScore {
		copy(ans, temp[1:n+1])
	}
	return
}

func main() {
	var n int
	fmt.Scan(&n)
	poss := make([]Pos, n)
	for i := range poss {
		fmt.Scan(&poss[i].x, &poss[i].y)
	}
	ans := Solve(poss)
	for _, p := range ans {
		fmt.Println(p.x, p.y)
	}
}
