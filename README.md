# paiza 8月13日はpaizaの日！怪盗paizaからの挑戦状 での俺の解答


https://paiza.jp/poh/phantom_thief  



------------------------------------------------------

ヒントに従ったそのままの提出: 635,537 m  
./main.go  
https://paiza.jp/poh/phantom_thief/result/3016b7e9  



第二のヒントに従いランダムで提出: 642,675 m  
./random/main.go  
https://paiza.jp/poh/phantom_thief/result/56822f53  



スタート地点から近い順に移動: 378,752 m  
./nearer_start/main.go  
https://paiza.jp/poh/phantom_thief/result/805a94df  



現在地点から近い順に移動: 144,850 m  
./nearest_current/main.go  
https://paiza.jp/poh/phantom_thief/result/be9a6b21  



適当に交換して探す: 196,365 m  
./swapping/main.go  
https://paiza.jp/poh/phantom_thief/result/fe223941  



シャッフルと適当に交換して探す： 172,995 m  
./shuffle_swapping/main.go  
https://paiza.jp/poh/phantom_thief/result/d30f0d56  



ただシャッフルを繰り返すだけ: 464,293 m  
./repeat_shuffle/main.go  
https://paiza.jp/poh/phantom_thief/result/5b6b739c  



現在地から遠い順に移動: 879,706 m  
./farthest_current/main.go  
https://paiza.jp/poh/phantom_thief/result/0c3d992a  



現在地点から近い順に移動するルートに対して適当に交換(~~これ山登り法っぽい~~): 141,297 m  
./nearest_after_swapping/main.go  
https://paiza.jp/poh/phantom_thief/result/399903a0  



現在地近い順移動ルートに対して適当に3個交換2個交換(~~これ山登り法っぽい~~): 137,490 m  
./nearest_after_swapping3/main.go  
https://paiza.jp/poh/phantom_thief/result/38034aeb  



ビームサーチっぽい何か: 139,831 m  
./beam/main.go  
https://paiza.jp/poh/phantom_thief/result/946df495  



焼きなまし法っぽい何か:  141,400 m  
./sim_annealing/main.go  
https://paiza.jp/poh/phantom_thief/result/bc0a5ce7  



焼きなまし法っぽい何か(3個交換): 138,989 m  
./sim_annealing_swapping3/main.go  
https://paiza.jp/poh/phantom_thief/result/d79a42c7   



焼きなまし法っぽい何か(ランダム選択じゃなく順番に): 141,672 m  
./sim_annealing_liner/main.go  
https://paiza.jp/poh/phantom_thief/result/42d2fa73  



焼きなまし法っぽい何か(2個&3個交換):  141,240 m  
./sim_annealing_swapping3ex/main.go  
https://paiza.jp/poh/phantom_thief/result/74ef8154  



焼きなまし法っぽい何か(2～5個交換): 141,240 m  
./sim_annealing5432/main.go  
https://paiza.jp/poh/phantom_thief/result/6ca18cc4  



焼きなまし法っぽい何か(近い点だけを交換？): 143,123 m  
./sim_annealing_near/main.go  
https://paiza.jp/poh/phantom_thief/result/9eb74365  



焼きなまし法っぽい何かと~~山登り法ぽい~~何か: 136,643 m  
./sim_annealing_and_hill_climbing/main.go  
https://paiza.jp/poh/phantom_thief/result/8a90c9f1  



部分パス同士で最小距離を結んでいく感じ: 136,513 m  
./min_edge/main.go  
https://paiza.jp/poh/phantom_thief/result/b0276661  



部分パス同士で最小距離を結んだあと~~山登り法~~(?): 135,145 m   
./min_edge_and_hillclimb/main.go  
https://paiza.jp/poh/phantom_thief/result/d8ff05c5  



部分パス同士で最小距離を結んだあと~~山登り法~~(?)と焼きなまし法(?):  133,723 m  
./min_edge/main_and_HC_and_SA/main.go  
https://paiza.jp/poh/phantom_thief/result/17c873be  



焼きなまし法っぽい何かを少し高速化:  141,297 m  
./sim_annealing_opt/main.go  
https://paiza.jp/poh/phantom_thief/result/8a70df58  
