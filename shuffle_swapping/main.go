// Try paiza Phantom Thief
// author: Leonardone @ NEETSDKASU
package main

import (
	"fmt"
	"math"
	"math/rand"
)

type Pos struct{ x, y int }

func (a *Pos) Distance(b Pos) float64 {
	dx := float64(a.x - b.x)
	dy := float64(a.y - b.y)
	return math.Sqrt(dx*dx + dy*dy)
}

func DistanceNeighbours(poss []Pos, i int) float64 {
	return poss[i].Distance(poss[i-1]) + poss[i].Distance(poss[i+1])
}

func Swap(poss []Pos, i, j int) {
	poss[i], poss[j] = poss[j], poss[i]
}

func TotalDistance(poss []Pos) (dist float64) {
	for i, p := range poss[1:] {
		dist += p.Distance(poss[i])
	}
	return
}

func Shuffle(poss []Pos, begin, end int) {
	for i := begin; i < end; i++ {
		j := rand.Intn(end-begin+1) + begin
		poss[i], poss[j] = poss[j], poss[i]
	}
}

func Solve(poss []Pos) (ans []Pos) {
	n := len(poss)
	endElement := n - 1
	temp := []Pos{{0, 0}}
	temp = append(temp, poss...)
	ans = append(ans, poss...)
	totalMinimum := TotalDistance(temp)
	for t := 0; t < 10000; t++ {
		maximum := 0.0
		swapPos := Pos{}
		for i := 1; i < n-1; i++ {
			di := DistanceNeighbours(temp, i)
			for j := i + 1; j < n; j++ {
				dist := di + DistanceNeighbours(temp, j)
				Swap(temp, i, j)
				dist -= DistanceNeighbours(temp, i) + DistanceNeighbours(temp, j)
				Swap(temp, i, j)
				if math.Signbit(dist) {
					continue
				}
				if dist == math.Max(dist, maximum) {
					maximum = dist
					swapPos = Pos{i, j}
				}
			}
		}
		if swapPos.x == 0 {
			dist := TotalDistance(temp)
			if dist == math.Min(dist, totalMinimum) {
				totalMinimum = dist
				copy(ans, temp[1:])
			}
			copy(temp[1:], poss)
			endElement = (endElement + 1) % n
			Swap(temp, endElement+1, n)
			Shuffle(temp, 1, n-1)
			continue
		}
		Swap(temp, swapPos.x, swapPos.y)
	}
	return
}

func main() {
	var n int
	fmt.Scan(&n)
	poss := make([]Pos, n)
	for i := range poss {
		fmt.Scan(&poss[i].x, &poss[i].y)
	}
	ans := Solve(poss)
	for _, p := range ans {
		fmt.Println(p.x, p.y)
	}
}
