// Try paiza Phantom Thief
// author: Leonardone @ NEETSDKASU
package main

import (
	"fmt"
	"math"
)

type Pos struct{ x, y int }

type Path []int

type Edge struct {
	f, t       int
	flas, tlas bool
	dis        float64
}

func (p Pos) Distance(r Pos) float64 {
	dx := float64(p.x - r.x)
	dy := float64(p.y - r.y)
	return math.Sqrt(dx*dx + dy*dy)
}

func Solve(poss []Pos) (ans []Pos) {

	n := len(poss)
	poss = append(poss, Pos{0, 0})

	tmp := []Path{}
	for i := range poss {
		tmp = append(tmp, Path{i})
	}

	for len(tmp) > 1 {
		var e Edge
		e.dis = math.MaxFloat64
		for i, t1 := range tmp {
			for j, t2 := range tmp[i+1:] {
				d1 := poss[t1[0]].Distance(poss[t2[0]])
				if d1 < e.dis {
					e = Edge{f: i, t: j + i + 1, dis: d1}
				}
				if t1[len(t1)-1] != n {
					d2 := poss[t1[len(t1)-1]].Distance(poss[t2[0]])
					if d2 < e.dis {
						e = Edge{f: i, t: j + i + 1, flas: true, dis: d2}
					}
				}
				if t2[len(t2)-1] != n {
					d3 := poss[t1[0]].Distance(poss[t2[len(t2)-1]])
					if d3 < e.dis {
						e = Edge{f: i, t: j + i + 1, tlas: true, dis: d3}
					}
				}
				if t1[len(t1)-1] != n && t2[len(t2)-1] != n {
					d4 := poss[t1[len(t1)-1]].Distance(poss[t2[len(t2)-1]])
					if d4 < e.dis {
						e = Edge{f: i, t: j + i + 1, flas: true, tlas: true, dis: d4}
					}
				}
			}
		}
		tmp2 := []Path{}
		for i, t := range tmp {
			if e.f != i && e.t != i {
				tmp2 = append(tmp2, t)
			}
		}
		switch {
		case !e.flas && !e.tlas:
			if tmp[e.f][len(tmp[e.f])-1] == n {
				e.f, e.t = e.t, e.f
			}
			xs := tmp[e.f]
			for i, j := 0, len(xs)-1; i < j; i, j = i+1, j-1 {
				xs[i], xs[j] = xs[j], xs[i]
			}
			xs = append(xs, tmp[e.t]...)
			tmp2 = append(tmp2, xs)
		case e.flas && !e.tlas:
			xs := append(tmp[e.f], tmp[e.t]...)
			tmp2 = append(tmp2, xs)
		case !e.flas && e.tlas:
			xs := append(tmp[e.t], tmp[e.f]...)
			tmp2 = append(tmp2, xs)
		case e.flas && e.tlas:
			xs := tmp[e.f]
			for i := range tmp[e.t] {
				xs = append(xs, tmp[e.t][len(tmp[e.t])-1-i])
			}
			tmp2 = append(tmp2, xs)
		}
		tmp = tmp2
	}

	for i := n - 1; i >= 0; i-- {
		ans = append(ans, poss[tmp[0][i]])
	}

	return
}

func main() {
	var n int
	fmt.Scan(&n)
	poss := make([]Pos, n)
	for i := range poss {
		fmt.Scan(&poss[i].x, &poss[i].y)
	}
	ans := Solve(poss)
	for _, p := range ans {
		fmt.Println(p.x, p.y)
	}
}
