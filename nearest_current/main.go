// Try paiza Phantom Thief
// author: Leonardone @ NEETSDKASU
package main

import (
	"fmt"
	"math"
	"sort"
)

type Pos struct{ x, y int }

func (a *Pos) Distance(b Pos) float64 {
	dx := float64(a.x - b.x)
	dy := float64(a.y - b.y)
	return math.Sqrt(dx*dx + dy*dy)
}

func Solve(poss []Pos) (ans []Pos) {
	current := Pos{0, 0}
	for len(poss) > 0 {
		sort.Slice(poss, func(i, j int) bool {
			di := current.Distance(poss[i])
			dj := current.Distance(poss[j])
			return di < dj
		})
		current = poss[0]
		ans = append(ans, current)
		poss = poss[1:]
	}
	return
}

func main() {
	var n int
	fmt.Scan(&n)
	poss := make([]Pos, n)
	for i := range poss {
		fmt.Scan(&poss[i].x, &poss[i].y)
	}
	ans := Solve(poss)
	for _, p := range ans {
		fmt.Println(p.x, p.y)
	}
}
