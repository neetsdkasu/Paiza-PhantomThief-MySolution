// Try paiza Phantom Thief
// author: Leonardone @ NEETSDKASU
package main

import (
	"fmt"
	"math"
	"math/rand"
	"sort"
	"time"
)

type Pos struct{ x, y int }

func (a *Pos) Distance(b Pos) float64 {
	dx := float64(a.x - b.x)
	dy := float64(a.y - b.y)
	return math.Sqrt(dx*dx + dy*dy)
}

func DistanceNeighbours(poss []Pos, i int) float64 {
	return poss[i].Distance(poss[i-1]) + poss[i].Distance(poss[i+1])
}

func Swap(poss []Pos, i, j int) {
	poss[i], poss[j] = poss[j], poss[i]
	if n := len(poss); j == n-2 {
		poss[n-1] = poss[j]
	}
}

func OrderToNearestCurrent(poss []Pos) (res []Pos) {
	current := Pos{0, 0}
	for len(poss) > 0 {
		sort.Slice(poss, func(i, j int) bool {
			di := current.Distance(poss[i])
			dj := current.Distance(poss[j])
			return di < dj
		})
		current = poss[0]
		res = append(res, current)
		poss = poss[1:]
	}
	return
}

func calcScore(poss []Pos) float64 {
	score := float64(0)
	for i, p := range poss[1:] {
		score += p.Distance(poss[i])
	}
	return score
}

func Solve(poss []Pos) (ans []Pos) {
	limit := time.After(2900 * time.Millisecond)
	poss = OrderToNearestCurrent(poss)
	n := len(poss)
	ans = make([]Pos, n)
	copy(ans, poss)
	temp := []Pos{{0, 0}}
	temp = append(temp, poss...)
	temp = append(temp, poss[n-1])
	bestScore := calcScore(temp)
	startScore := bestScore
	score := bestScore
	rand.Seed(19831983)
	const Alpha = 0.8
	const maxloop = 8000000
mainloop:
	for t := 0; t < maxloop; t++ {
		select {
		case <-limit:
			break mainloop
		default:
		}

		i := rand.Intn(n) + 1
		j := rand.Intn(n) + 1
		if i == j {
			j = i%n + 1
		}
		Swap(temp, i, j)
		tmpScore := calcScore(temp)
		if tmpScore < bestScore {
			score = tmpScore
			startScore = tmpScore
			bestScore = tmpScore
			copy(ans, temp[1:n+1])
			continue
		}
		if tmpScore < startScore {
			score = tmpScore
			startScore = tmpScore
			continue
		}
		te := math.Pow(Alpha, float64(t)/maxloop)
		ee := math.Exp((startScore - tmpScore) / te)
		if rand.Float64() < ee {
			score = tmpScore
			startScore = tmpScore
			continue
		}
		Swap(temp, i, j)
	}
	score = calcScore(temp)
	if score < bestScore {
		copy(ans, temp[1:n+1])
	}
	return
}

func main() {
	var n int
	fmt.Scan(&n)
	poss := make([]Pos, n)
	for i := range poss {
		fmt.Scan(&poss[i].x, &poss[i].y)
	}
	ans := Solve(poss)
	for _, p := range ans {
		fmt.Println(p.x, p.y)
	}
}
