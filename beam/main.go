// Try paiza Phantom Thief
// author: Leonardone @ NEETSDKASU
package main

import (
	"container/heap"
	"fmt"
	"math"
	"sort"
)

type Pos struct{ x, y float64 }

func (p Pos) Distance(e Pos) float64 {
	dx, dy := p.x-e.x, p.y-e.y
	return math.Sqrt(dx*dx + dy*dy)
}

type Path struct {
	index int
	prev  *Path
}

type Flag struct {
	lf, hf uint64
}

func (f *Flag) Exist(i int) bool {
	if i < 64 {
		return (f.lf>>uint(i))&1 != 0
	} else {
		return (f.hf>>uint(i&63))&1 != 0
	}
}

func (f *Flag) Raise(i int) {
	if i < 64 {
		f.lf |= 1 << uint(i)
	} else {
		f.hf |= 1 << uint(i&63)
	}
}

type State struct {
	distance float64
	current  Pos
	visited  Flag
	path     *Path
}

type PQ []*State

func (pq PQ) Len() int            { return len(pq) }
func (pq PQ) Less(i, j int) bool  { return pq[i].distance < pq[j].distance }
func (pq PQ) Swap(i, j int)       { pq[i], pq[j] = pq[j], pq[i] }
func (pq *PQ) Push(x interface{}) { *pq = append(*pq, x.(*State)) }
func (pq *PQ) Pop() interface{} {
	n := len(*pq) - 1
	res := (*pq)[n]
	*pq = (*pq)[:n]
	return res
}

func Solve(poss []Pos) (ans []Pos) {
	pqs := make([]PQ, len(poss)+10)
	var sp Pos
	sort.Slice(poss, func(i, j int) bool {
		return sp.Distance(poss[i]) < sp.Distance(poss[j])
	})
	for i, p := range poss {
		st := new(State)
		st.distance = sp.Distance(p)
		st.current = p
		st.visited.Raise(i)
		path := Path{i, nil}
		st.path = &path
		heap.Push(&pqs[0], st)
	}
	loop := 3500 * 100 / len(poss)
	beam := 80 * 100 / len(poss)
	off := 0
	for k := 0; k < loop; k++ {
		for off < len(poss) && len(pqs[off]) == 0 {
			off++
		}
		for i := off; i < len(poss)-1; i++ {
			cs := heap.Pop(&pqs[i]).(*State)
			for j, p := range poss {
				if cs.visited.Exist(j) {
					continue
				}
				tmp := *cs
				tmp.visited.Raise(j)
				tmp.distance += p.Distance(cs.current)
				tmp.current = p
				path := Path{j, cs.path}
				tmp.path = &path
				heap.Push(&pqs[i+1], &tmp)
			}
			if len(pqs[i+1]) > beam {
				pqs[i+1] = pqs[i+1][:beam]
			}
		}
	}
	prelas := pqs[len(poss)-2]
	lasi := len(poss) - 1
	for prelas.Len() > 0 {
		cs := heap.Pop(&prelas).(*State)
		for j, p := range poss {
			if cs.visited.Exist(j) {
				continue
			}
			tmp := *cs
			tmp.visited.Raise(j)
			tmp.distance += p.Distance(cs.current)
			tmp.current = p
			path := Path{j, cs.path}
			tmp.path = &path
			heap.Push(&pqs[lasi], &tmp)
		}
	}
	if len(pqs[len(poss)-1]) == 0 {
		ans = poss
		return
	}
	res := heap.Pop(&pqs[len(poss)-1]).(*State)
	ans = make([]Pos, len(poss))
	path := res.path
	for j := range ans {
		ans[len(ans)-j-1] = poss[path.index]
		path = path.prev
	}
	return
}

func main() {
	var n int
	fmt.Scan(&n)
	poss := make([]Pos, n)
	for i := range poss {
		fmt.Scan(&poss[i].x, &poss[i].y)
	}
	ans := Solve(poss)
	for _, p := range ans {
		fmt.Println(p.x, p.y)
	}
}
