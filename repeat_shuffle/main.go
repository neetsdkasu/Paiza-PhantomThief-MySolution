// Try paiza Phantom Thief
// author: Leonardone @ NEETSDKASU
package main

import (
	"fmt"
	"math"
	"math/rand"
)

type Pos struct{ x, y int }

func (a *Pos) Distance(b Pos) float64 {
	dx := float64(a.x - b.x)
	dy := float64(a.y - b.y)
	return math.Sqrt(dx*dx + dy*dy)
}

func TotalDistance(poss []Pos) (dist float64) {
	for i, p := range poss[1:] {
		dist += p.Distance(poss[i])
	}
	return
}

func Shuffle(poss []Pos, begin, end int) {
	for i := begin; i < end; i++ {
		j := rand.Intn(end-begin+1) + begin
		poss[i], poss[j] = poss[j], poss[i]
	}
}

func Solve(poss []Pos) (ans []Pos) {
	n := len(poss)
	temp := []Pos{{0, 0}}
	temp = append(temp, poss...)
	ans = append(ans, poss...)
	totalMinimum := TotalDistance(temp)
	for t := 0; t < 550000; t++ {
		Shuffle(temp, 1, n)
		dist := TotalDistance(temp)
		if dist == math.Min(dist, totalMinimum) {
			totalMinimum = dist
			copy(ans, temp[1:])
		}
	}
	return
}

func main() {
	var n int
	fmt.Scan(&n)
	poss := make([]Pos, n)
	for i := range poss {
		fmt.Scan(&poss[i].x, &poss[i].y)
	}
	ans := Solve(poss)
	for _, p := range ans {
		fmt.Println(p.x, p.y)
	}
}
